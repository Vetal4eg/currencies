# frozen_string_literal: true

FactoryBot.define do
  factory :currency do
    name { FFaker::Currency.unique.name }
    value { rand(1.0..100.0).ceil(4) }
  end
end
