# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CurrenciesController, type: :controller do
  before do
    expect(controller).to receive(:authenticate).and_return(true)
  end

  describe '#index' do
    let!(:currencies) { FactoryBot.create_list(:currency, 100) }

    it 'return currencies' do
      get :index

      json_response = JSON.parse(response.body)

      currency = currencies.first
      expect(json_response['currencies'].first['id']).to eq currency.id
      expect(json_response['currencies'].first['name']).to eq currency.name
      expect(json_response['currencies'].first['value']).to eq currency.value

      expect(json_response['pagy']['count']).to eq(100)
      expect(json_response['pagy']['items']).to eq(10)
    end
  end

  describe '#show' do
    let(:currency) { FactoryBot.create(:currency) }

    it 'return currency value' do
      get(:show, params: { id: currency.id })

      json_response = JSON.parse(response.body)
      expect(json_response['value']).to eq currency.value
    end
  end
end
