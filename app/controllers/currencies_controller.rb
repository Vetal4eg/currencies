# frozen_string_literal: true

class CurrenciesController < ApplicationController
  include Pagy::Backend

  MAX_PER_PAGE = 10

  def index
    @pagy, @currencies = pagy(Currency.select(:id, :name, :value), items: MAX_PER_PAGE)

    render json: { currencies: @currencies, pagy: pagy_metadata(@pagy) }
  end

  def show
    @currency = Currency.select(:id, :value).find(params[:id])

    render json: @currency
  end
end
