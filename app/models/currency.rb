# frozen_string_literal: true

class Currency < ApplicationRecord
  validates :name, presence: true, uniqueness: true
  validates :value, presence: true
end
