# frozen_string_literal: true

Rails.application.routes.draw do
  resources :currencies, only: %i[index show]
end
