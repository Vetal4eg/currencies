# frozen_string_literal: true

URL = 'http://www.cbr.ru/scripts/XML_daily.asp'

response = HTTParty.get(URL).parsed_response

response['ValCurs']['Valute'].each do |сurrency|
  Currency.create(name: сurrency['Name'], value: сurrency['Value'].gsub(',', '.').to_f)
end
