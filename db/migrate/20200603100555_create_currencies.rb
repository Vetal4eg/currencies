# frozen_string_literal: true

class CreateCurrencies < ActiveRecord::Migration[6.0]
  def change
    create_table :currencies do |t|
      t.string :name
      t.float :value

      t.timestamps
    end

    add_index :currencies, :name, unique: true
  end
end
